import unittest
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time
 
class ServiceTest(unittest.TestCase):
 
    @classmethod
    def setUpClass(cls):
        #cls.driver = webdriver.Firefox(executable_path='/tmp/geckodriver')
        cls.driver = webdriver.PhantomJS()
 
    def test_title(self):
        self.driver.get('http://localhost:8000')
        self.assertEqual(self.driver.title,'Sultan Maiyaki')
 
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()
